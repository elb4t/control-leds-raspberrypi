<?php

  if ($_REQUEST[blink]) { 
    apagarLeds();
    exec("sudo python /var/www/html/scripts/blink.py 2>/dev/null >/dev/null &");
    echo '{"success":"blink"}';
    return;
  }

  if ($_REQUEST[pwm]) { 
    apagarLeds();
    exec("sudo python /var/www/html/scripts/pwm.py 2>/dev/null >/dev/null &");
    echo '{"success":"pwm"}';
    return;
  }

  if ($_REQUEST[ruleta]) { 
    apagarLeds();
    exec("sudo python /var/www/html/scripts/ruleta.py 2>/dev/null >/dev/null &");
    echo '{"success":"ruleta"}';
    return;
  }

  if ($_REQUEST[auto]) { 
    apagarLeds();
    exec("sudo python /var/www/html/scripts/auto.py 2>/dev/null >/dev/null &");
    echo '{"success":"auto"}';
    return;
  }

  if ($_REQUEST[parpadeo]) { 
    exec("sudo pkill -f /var/www/html/scripts/parpadeo.py");
    sleep(0.5);
    exec("sudo python /var/www/html/scripts/parpadeo.py 2>/dev/null >/dev/null &");
    echo '{"success":"parpadeo"}';
    return;
  }

  if ($_REQUEST[stop]) { 
    apagarLeds();
    echo '{"success":"stop"}';
    return;
  }

  function apagarLeds(){
    exec("sudo pkill -f /var/www/html/scripts/blink.py");
    exec("sudo pkill -f /var/www/html/scripts/ruleta.py");
    exec("sudo pkill -f /var/www/html/scripts/pwm.py");
    if ($_REQUEST[stopall]) { 
      exec("sudo pkill -f /var/www/html/scripts/auto.py");
      exec("sudo pkill -f /var/www/html/scripts/parpadeo.py");
    }
    sleep(0.5);
    if ($_REQUEST[stopall]) {
      exec("sudo python /var/www/html/scripts/stopall.py 2>/dev/null >/dev/null &");
    }else{
      exec("sudo python /var/www/html/scripts/stop.py 2>/dev/null >/dev/null &");
    }
    sleep(0.5);
  }
?>