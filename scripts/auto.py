import time
import requests 

URL = "http://localhost/api/"
DELAY = 300

def request_api(llamada):
    r = requests.post(url = URL, data = {llamada:'true'})
    #print("Respuesta: %s"%r.text)
    time.sleep(0.5)

try:
    request_api('parpadeo')
    while True:
        #Llamada a pwm
        request_api('stop')
        request_api('pwm')
        time.sleep(DELAY)

        #Llamada a ruleta
        request_api('stop')
        request_api('ruleta')
        time.sleep(DELAY)

        #Llamada a blink
        request_api('stop')
        request_api('blink')
        time.sleep(DELAY)

except KeyboardInterrupt:   # Se ha pulsado CTRL+C!!
    request_api('stop')