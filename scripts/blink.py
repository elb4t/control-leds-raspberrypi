#! /usr/bin/env python
import RPi.GPIO as GPIO #importamos la libreria y cambiamos su nombre por "GPIO"
import time #necesario para los delays
import leds as LED #importamos los pines de los LEDs
 
try:
    while True:
    
        GPIO.output(LED.ROJO, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(LED.ROJO, GPIO.LOW)
        GPIO.output(LED.RGB_NARANJA, GPIO.HIGH)
        time.sleep(1)


        GPIO.output(LED.NARANJA, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(LED.NARANJA, GPIO.LOW)
        GPIO.output(LED.RGB_NARANJA, GPIO.LOW)
        GPIO.output(LED.RGB_AZUL, GPIO.HIGH)
        time.sleep(1)

        GPIO.output(LED.VERDE, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(LED.RGB_AZUL, GPIO.LOW)
        GPIO.output(LED.VERDE, GPIO.LOW)
        time.sleep(1) 

        #Rueda
        GPIO.output(LED.RGB_AZUL, GPIO.HIGH)
        GPIO.output(LED.RGB_NARANJA, GPIO.HIGH)
        GPIO.output(LED.RGB_ROJO, GPIO.HIGH)
        GPIO.output(LED.ROJO, GPIO.HIGH)
        GPIO.output(LED.ROJO, GPIO.HIGH)
        time.sleep(.2)
        GPIO.output(LED.ROJO, GPIO.LOW)
        time.sleep(.2)
        GPIO.output(LED.NARANJA, GPIO.HIGH)
        time.sleep(.2)
        GPIO.output(LED.NARANJA, GPIO.LOW)
        time.sleep(.2)
        GPIO.output(LED.VERDE, GPIO.HIGH)
        time.sleep(.2)
        GPIO.output(LED.VERDE, GPIO.LOW)
        time.sleep(.2)
        GPIO.output(LED.ROJO, GPIO.HIGH)
        time.sleep(.2)
        GPIO.output(LED.ROJO, GPIO.LOW)
        time.sleep(.2)
        GPIO.output(LED.NARANJA, GPIO.HIGH)
        time.sleep(.2)
        GPIO.output(LED.NARANJA, GPIO.LOW)
        time.sleep(.2)
        GPIO.output(LED.VERDE, GPIO.HIGH)
        time.sleep(.2)
        GPIO.output(LED.VERDE, GPIO.LOW)
        time.sleep(.2)
        GPIO.output(LED.RGB_AZUL, GPIO.LOW)
        GPIO.output(LED.RGB_NARANJA, GPIO.LOW)
        GPIO.output(LED.RGB_ROJO, GPIO.LOW)


except KeyboardInterrupt:   # Se ha pulsado CTRL+C!!
    GPIO.output(LED.ROJO, GPIO.LOW)
    GPIO.output(LED.NARANJA, GPIO.LOW)
    GPIO.output(LED.RGB_AZUL, GPIO.LOW)
