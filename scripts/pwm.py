#! /usr/bin/env python
import RPi.GPIO as GPIO # Cargamos la libreria RPi.GPIO
import time
import leds as LED #importamos los pines de los LEDs
import threading


def pwmled(color):
    while True:
        for i in range(0,101):            # De i=0 hasta i=101 (101 porque el script se detiene al 100%)
            color.ChangeDutyCycle(i) 
            time.sleep(pause_time)
        for i in range(100,-1,-1):        # Desde i=100 a i=0 en pasos de -1
            color.ChangeDutyCycle(i)
            time.sleep(pause_time)



# Creamos los objetos para cada color
naranja = GPIO.PWM(LED.NARANJA, 100)
red = GPIO.PWM(LED.ROJO, 100)     
green = GPIO.PWM(LED.VERDE, 100) 
rgb_red = GPIO.PWM(LED.RGB_ROJO, 100)
rgb_naranja = GPIO.PWM(LED.RGB_NARANJA, 100)     
rgb_azul = GPIO.PWM(LED.RGB_AZUL, 100) 

# Iniciamos el objeto (completamente apagado)
naranja.start(0)              
red.start(0)            
green.start(0)

rgb_red.start(0)              
rgb_azul.start(0)            
rgb_naranja.start(0)
# A partir de ahora empezamos a modificar los valores del ciclo de trabajo

pause_time = 0.02           # Declaramos un lapso de tiempo para las pausas


try:                        # Abrimos un bloque 'Try...except KeyboardInterrupt'
    t = threading.Thread(target=pwmled, args=(naranja,))
    t.start()
    time.sleep(1.5)
    t = threading.Thread(target=pwmled, args=(red,))
    t.start()
    time.sleep(1.5)
    t = threading.Thread(target=pwmled, args=(green,))
    t.start()
    time.sleep(1.5)

    t = threading.Thread(target=pwmled, args=(rgb_red,))
    t.start()
    time.sleep(1.5)
    t = threading.Thread(target=pwmled, args=(rgb_azul,))
    t.start()
    time.sleep(1.5)
    t = threading.Thread(target=pwmled, args=(rgb_naranja,))
    t.start()
    time.sleep(1.5)
    #thread.start_new_thread(self.pwm_led, (red,))
        
except KeyboardInterrupt:   # Se ha pulsado CTRL+C!!
    naranja.stop()
    red.stop()
    green.stop()

    rgb_red.stop()           
    rgb_azul.stop()
    rgb_naranja.stop()
    GPIO.cleanup()

