#! /usr/bin/env python
import RPi.GPIO as GPIO #importamos la libreria y cambiamos su nombre por "GPIO"

#Definicion de los pines para cada color
BLANCO = 4 #Pin GPIO4

ROJO = 17 #Pin GPIO17
NARANJA = 27 #Pin GPIO27
VERDE = 22 #Pin GPIO22

RGB_ROJO = 13 #Pin GPIO13
RGB_NARANJA = 19 #Pin GPIO19
RGB_AZUL = 26 #Pin GPIO26

#establecemos el sistema de numeracion que queramos, en mi caso BCM
GPIO.setmode(GPIO.BCM)

#configuramos los pines como una salida
GPIO.setup(BLANCO, GPIO.OUT)

GPIO.setup(RGB_ROJO, GPIO.OUT)
GPIO.setup(RGB_NARANJA, GPIO.OUT)
GPIO.setup(RGB_AZUL, GPIO.OUT)

GPIO.setup(NARANJA, GPIO.OUT)
GPIO.setup(VERDE, GPIO.OUT)
GPIO.setup(ROJO, GPIO.OUT)