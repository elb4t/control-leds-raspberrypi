import RPi.GPIO as GPIO # Cargamos la libreria RPi.GPIO
import time
import leds as LED #importamos los pines de los LEDs


blanco = GPIO.PWM(LED.BLANCO, 100)   # Creamos el objeto 'white' a 100 Hz

blanco.start(100)              # Iniciamos el objeto 'white' al 0% del ciclo de trabajo (completamente apagado)

# A partir de ahora empezamos a modificar los valores del ciclo de trabajo

pause_time = 0.02           # Declaramos un lapso de tiempo para las pausas

try:                        # Abrimos un bloque 'Try...except KeyboardInterrupt'
    while True:             # Iniciamos un bucle 'while true'
        for i in range(0,5): 
            time.sleep(5)
            for i in range(100,-1,-10):        # Desde i=100 a i=0 en pasos de -1
                blanco.ChangeDutyCycle(i)      # LED #1 = i
                time.sleep(pause_time)             # Pequena pausa para no saturar el procesador
            for i in range(0,101,10):            # De i=0 hasta i=101 (101 porque el script se detiene al 100%)
                blanco.ChangeDutyCycle(i)  # LED #2 resta 100 - i
                time.sleep(pause_time)             # Pequena pausa para no saturar el procesador
        #Parpadea dos veces
        time.sleep(5)
        for i in range(0,2): 
            for i in range(100,-1,-10):        # Desde i=100 a i=0 en pasos de -1
                blanco.ChangeDutyCycle(i)      # LED #1 = i
                time.sleep(pause_time)             # Pequena pausa para no saturar el procesador
            for i in range(0,101,10):            # De i=0 hasta i=101 (101 porque el script se detiene al 100%)
                blanco.ChangeDutyCycle(i)  # LED #2 resta 100 - i
                time.sleep(pause_time)             # Pequena pausa para no saturar el procesador
except KeyboardInterrupt:   # Se ha pulsado CTRL+C!!
    blanco.stop()
    GPIO.cleanup()

