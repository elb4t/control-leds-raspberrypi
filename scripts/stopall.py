#! /usr/bin/env python
import RPi.GPIO as GPIO #importamos la libreria y cambiamos su nombre por "GPIO"
import leds as LED #importamos los pines de los LEDs

GPIO.output(LED.BLANCO, GPIO.LOW)
GPIO.output(LED.ROJO, GPIO.LOW)
GPIO.output(LED.NARANJA, GPIO.LOW)
GPIO.output(LED.VERDE, GPIO.LOW)
GPIO.output(LED.RGB_AZUL, GPIO.LOW)
GPIO.output(LED.RGB_NARANJA, GPIO.LOW)
GPIO.output(LED.RGB_ROJO, GPIO.LOW)
GPIO.cleanup()