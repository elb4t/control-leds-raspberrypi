<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Control LEDs</title>
  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <link rel="stylesheet" href="/css/style.css">
</head>
 <body>
   <p><input type="button" class="btn" id="parpadeo" value="Parpadeo" onclick="realizaProceso('parpadeo')"></p>
   <p><input type="button" class="btn" id="blink" value="Blink" onclick="realizaProceso('blink')"></p>
   <p><input type="button" class="btn" id="pwm" value="PWM" onclick="realizaProceso('pwm')"></p>
   <p><input type="button" class="btn" id="ruleta" value="Ruleta" onclick="realizaProceso('ruleta')"></p>
   <p><input type="button" class="btn" id="auto" value="Auto" onclick="realizaProceso('auto')"></p>
   <p><input type="button" class="btn danger" id="stop" value="STOP" onclick="realizaProceso('stop')"></p>


 </body>
 <script type="text/javascript">
  function realizaProceso(llamada){
    var parametros = '{"' +llamada+'":true';
    if (llamada == 'stop'){
      parametros += ', "stopall": true';
    }
    parametros += '}';
    
    $.ajax({
        type: "POST",
        url: '/api/',
        data: JSON.parse(parametros),
        success: function(response)
        {
            console.log(response);
            var jsonData = JSON.parse(response);

            if (jsonData.success)
            {
              $("input").removeClass("active");
              $('#'+jsonData.success).addClass("active");
            }
            else
            {
                alert('Error!');
            }
        }
    });
  };
</script>
</html>